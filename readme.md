#### Requirements
- Python 3.x
- pip 3.x

##### Steps to run unit testing
- cd src/tools
- python -m unittest comparison_test search_test


#### Third party tools
###### pyenv
Python Environment selector based on the folder.
Follow the instructions in this webpage to install and setup.
https://github.com/pyenv/pyenv

Before configuring the python version, ensure the python version is installed using pyenv.
To get the list of version supported and able to install
- pyenv install --list
Choose one of the version and install it
- pyenv install {version code}

Go to the project folder
- cd {project-folder}
Configure the python version to run for this folder
- pyenv local {version code}


#### References
https://stackoverflow.com/questions/7225900/how-to-install-packages-using-pip-according-to-the-requirements-txt-file-from-a

