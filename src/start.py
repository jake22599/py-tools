# -*- coding: utf-8 -*-
import unittest

import tools  


def SearchForNonEnglishTextCSFile( file_path ):
    return_results = []

    ################################################################
    # Gather the code files
    regex = ".cs\Z"
    # get all the files in the folder path
    files = tools.GatherContentRecursively(file_path, regex)

    ################################################################
    # Get the strings that is enclosed in "xxx"

    regex = "[^a-zA-Z0-9!@#$%^&*()-=_+,.?\"':{}|<>_\[\]\\ \r\n]"
    delimiter = "\""

    for index, filePath in enumerate(files, 1):
        # open the file
        with open( filePath, 'r' ) as f:
            for lineNum, line in enumerate(f, 1):
                # Get the strings that is enclosed in "xxx"
                results = tools.GatherStringifyTextWithSingleDelimiter( line, delimiter )
                # check if the strings in the array fits the regular expressions
                results = tools.GatherStringInstance(results, regex)

                if( 0 != len(results) ):
                    return_results.extend( results ) 
            # end file content loop
        # end of file open

    ################################################################
    return return_results
# function end; SearchForNonEnglishTextCSFile

def CopyMissingFileStructure(source_path, other_path):
    import os
    from shutil import copy2

    # gather all files and folder in the source path
    sourceContents = tools.GatherContentRecursively( source_path )
    otherContents = tools.GatherContentRecursively( other_path )
    # remove the relative path
    sourceContents = tools.StripRelativePath(sourceContents, source_path + tools.PATH_SEPARATOR);
    otherContents = tools.StripRelativePath(otherContents, other_path + tools.PATH_SEPARATOR);

    # print( "sourceContents: " )
    # print( sourceContents )
    # print( "otherContents: " )
    # print( otherContents )

    # compare the source and existing sources files exist and check the files using the other path as parent path
    folders, files = tools.SplitFoldersAndFiles( sourceContents, otherContents, other_path )
    # print( "folders: " )
    # print( folders )
    # print( "files: " )
    # print( files )

    # Sort the array with least number of characters in front
    missing_array = folders.keys()
    missing_array.sort( key=len )
    # loop through the folder list
    for folder_path in missing_array:
        # construct the full folder path
        folderFullPath = other_path + tools.PATH_SEPARATOR + folder_path
        # create the missing folders, if it does not exist
        os.mkdir( folderFullPath )

    # loop through the missing file list
    for missing_content in files:
        # construct the full path for the source and destination path
        sourcePath = source_path + tools.PATH_SEPARATOR + missing_content;
        otherPath = other_path + tools.PATH_SEPARATOR + missing_content;

        print( "sourcePath: " + sourcePath );
        print( "otherPath: " + otherPath );

        # copy the file
        copy2(sourcePath, otherPath)
# function end; CopyMissingFileStructure

################################################################
################################################################
################################################################


################################################################
## WORKING ##
# look for non english text that is enclosed in "xxx"
# path = '/Users/eugene_lim/lapiww/jp/Assets/Scripts'
# results = SearchForNonEnglishTextCSFile( path )
################################################################
## WORKING ##
# copy the missing files and folders from the source to the destination.
# source_path = '/Users/eugene_lim/lapiww/jp/Assets/ResourcesDLC/Localize/ja'
# other_path = '/Users/eugene_lim/lapiww/jp/Assets/ResourcesDLC/Localize/en'

# # copy the missing files from the source path to the destination
# CopyMissingFileStructure( source_path, other_path )
################################################################
