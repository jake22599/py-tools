# -*- coding: utf-8 -*-
import os, re
from shutil import copy2
import constant

# gather text within a set of delimiters; "xxx", 'abc', ~123~, !efg!
def GatherStringifyTextWithSingleDelimiter( text, delimiter ):
    resultArray = []

    # get the length of the text
    textLength = len( text )
    index = 0
    startPosChar = -1

    # loop through the whole text
    while (index < textLength):
        # get the character at the index
        char = text[index]

        # check if the character is the same as the delimiter
        if char == delimiter:        
            # check for the first delimiter
            if startPosChar == -1:
                startPosChar = index
            else: # check for the second delimiter, to mark the end of the current set
                startIndex = startPosChar + 1
                # ensure the start index is not after the end of the line
                if( startIndex < textLength):
                    # get the text and add to the array
                    content = text[startIndex:index]
                    resultArray.append( content )

                # reset the start position variable
                startPosChar = -1
        # move to the next index
        index = index + 1
    # end of while loop

    return resultArray
# function end; GatherStringifyText

# gather text within a set of delimiters; {xxx}, [abc], <123>, (efg)
def GatherStringifyText( text, startDelimiter, endDelimiter ):
    resultArray = []

    INVALID_POS_VALUE = -1

    # get the length of the text
    textLength = len( text )
    index = 0
    startPosChar = INVALID_POS_VALUE
    endPosChar = INVALID_POS_VALUE

    # loop through the whole text
    while (index < textLength):
        # get the character at the index
        char = text[index]

        # check for the first delimiter character
        if startPosChar == INVALID_POS_VALUE and char == startDelimiter:
            startPosChar = index
        elif startPosChar != INVALID_POS_VALUE and char == endDelimiter:
            # check for the second delimiter, to mark the end of the current set
            startIndex = startPosChar + 1
            # ensure the start index is not after the end of the line
            if( startIndex < textLength):
                # get the text and add to the array
                content = text[startIndex:index]
                resultArray.append( content )

            # reset the start position variable
            startPosChar = -1
        # end of if/else
        # move to the next index
        index = index + 1
    # end of while loop

    return resultArray
# function end; GatherStringifyText

def GatherStringInstance(stringArray, regex):
    # array to store all the results
    results = {}

    # loop through the string array
    for index, text in enumerate(stringArray, 1):
        # LOOK for the string that fulfil the regular expression requirements
        regex_result = re.search(regex, text)
        if( None != regex_result ):
            results[ text ] = 1
    return results.keys()
# function end; GatherStringInstance

#########################################################################################################
# WORK IN PROGRESS
#########################################################################################################
