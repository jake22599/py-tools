﻿/**************************************************************************/
/*! @file   DebugMenuItem_Application.cs
    @brief  アプリケーション管理クラス
***************************************************************************/
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

//=========================================================================
//. デバッグメニューアイテム(アプリ側)
//=========================================================================
namespace GFSYS
{
#if DEBUG_BUILD
    public partial class DebugMenu : UnityEngine.MonoBehaviour
    {
        //=========================================================================
        //. シーンタイプ
        //=========================================================================
        public enum SceneType
        {
            NONE                        = -1,
            ROOT                        ,
            TITLE                       ,
            HOME                        ,
            BATTLE                      ,
        }
        
        //=========================================================================
        //. アイテムタイプ
        //=========================================================================
        public enum ItemType
        {
            NONE                        = -1,   // 無し
            
            ROOT                        ,       // ルート
            
            LOG                         ,       // ログ
            DEBUG                       ,       // デバッグ
            DEBUG_PROFILER              ,       // デバッグプロファイラ
            VIEW                        ,       // ビュー
            ASSET                       ,       // アセット
            FPSDISP                     ,       // FPS表示
            DEVICE_INFO                 ,       // デバイス情報
            BUGSEND                     ,       // バグ報告
            HIERARCHY                   ,       // ヒエラルキー
            
            SYSTEM                      ,       // システムルート
            
            TITLE                       ,       // タイトルルート
            
            HOME                        ,       // ホームルート
            HOME_TESTMOVIE              ,       // ホームルート
            
            HOME_ARENA                  ,       // ホームアリーナルート
            HOME_ARENA_BP0              ,       // ホームアリーナ BP
            HOME_ARENA_BPFULL           ,       // ホームアリーナ BP
            
            HOME_GUILDBTL               ,       // ホームギルドバトルルート
            HOME_GUILDBTL_RESULT_CHECK  ,       // ホームギルドバトルリザルトチェック
            HOME_GUILDBTL_RESULT_CHECK2 ,       // ホームギルドバトルリザルトチェック(死なない)
            
            HOME_CLASS_MATCH            ,       // ホームクラスマッチルート
            HOME_CLASS_MATCH_VS_NPC_ON  ,       // ホームクラスマッチVSNPC戦ON
            HOME_CLASS_MATCH_VS_NPC_OFF ,       // ホームクラスマッチVSNPC戦OFF

            HOME_SE                     ,       // ホームSEチェック
            HOME_SE_TEST01              ,       // ホームSEチェック01
            
            HOME_MISSION                ,       // ホームミッションルート
            HOME_MISSION_PACK01         ,       // ホームミッション パック購入01
            HOME_MISSION_PACK02         ,       // ホームミッション パック購入02
            
            HOME_OTHER                  ,       // ホーム その他
            HOME_OTHER_VIPRANK          ,       // ホーム その他 VIPランク
            HOME_OTHER_CAPOVER          ,       // ホーム アイテム超過
            
            HOME_OTHER_DEBUG00          ,
            HOME_OTHER_DEBUG01          ,
            HOME_OTHER_DEBUG02          ,
            HOME_OTHER_DEBUG03          ,
            HOME_OTHER_DEBUG04          ,
            HOME_OTHER_DEBUG05          ,
            HOME_OTHER_DEBUG06          ,
            HOME_OTHER_DEBUG07          ,
            
            BATTLE                      ,       // バトルルート
            BATTLE_DEBUG                ,       // バトルデバッグ
            
            BATTLE_DRAW                 ,       // バトル描画
            BATTLE_DRAW_BLUR            ,       // ブラー
            BATTLE_DRAW_SNAP            ,       // スナップ
            BATTLE_DRAW_CAMERAMODE      ,       // カメラモード
            BATTLE_DRAW_DEPTHOFFSET     ,       // カメラデプスオフセット
            BATTLE_DRAW_SHADOWCAM       ,       // カメラ影
            
            BATTLE_SETTING              ,
            BATTLE_FLAG_AUTOPLAY        ,
            BATTLE_FLAG_FORCEUNITCONTROLL,
            BATTLE_FLAG_SKILL_USE       ,
            BATTLE_FLAG_MOVE_TO_ALL     ,
            BATTLE_FLAG_SPEED           ,       // 倍速
            BATTLE_CHANGE_GRID          ,       // グリッド変更
            BATTLE_CHANGE_TRANSSHADER   ,       // シェーダー変更
            
            BATTLE_CONFIRM              ,       // バトル確認
            BATTLE_CONFIRM_TEXT         ,       // テキストエフェクト
            BATTLE_CONFIRM_TALK         ,       // 会話テスト
            BATTLE_CONFIRM_WIN          ,       // 勝利演出
            BATTLE_CONFIRM_LOSE         ,       // 敗北演出
            BATTLE_CONFIRM_CONTINUE     ,       // コンティニュー
            BATTLE_CONFIRM_SOUND        ,       // コンティニュー
            BATTLE_CONFIRM_SUMMON       ,       // 召喚演出
            BATTLE_CONFIRM_SLEEP        ,       // スリープ状態
            
            BATTLE_WINDOW               ,       // ウィンド
            BATTLE_WINDOW_LOGLIST       ,       // ログリスト
            BATTLE_WINDOW_UNITPREVIEW   ,       // ユニットプレビュー
            
            TEMP_01                     ,       // 雑用1
            TEMP_02                     ,       // 雑用2
            TEMP_03                     ,       // 雑用3
            TEMP_04                     ,       // 雑用4
            
            MAX                         ,
        }
        
        private SceneType   m_SceneType     = SceneType.ROOT;
        private ItemBase[]  m_Items         = new ItemBase[ (int)ItemType.MAX ];
        
        //=========================================================================
        //. アプリ専用
        //=========================================================================
        #region アプリ専用
        
        /// ***********************************************************************
        /// <summary>
        /// デバッグメニューの表示/非表示切り替え入力
        /// </summary>
        /// ***********************************************************************
        private bool IsInput( UISimpleTouch.Data touch )
        {
            Vector2 min = new Vector2( 0, 0 );
            Vector2 max = new Vector2( ScreenUtility.WIDTH, m_Root.height );
            
            // シーンによって入力範囲を若干変える
            if( m_SceneType == SceneType.HOME )
            {
                // HOMEは左上にオプションボタンがあるのでそこを除外
                min.x += 64;
                
                // ログモニターが表示中は右端を除外
                if( DebugLogMonitor.isDisp )
                {
                    max.x -= 30;
                }
            }
            else if( m_SceneType == SceneType.BATTLE )
            {
                // BATTLEは右上にポーズボタンとオート設定があるのでそこを除外
                max.x -= 128;
            }
            
            // 入力範囲
            touch.SetGuiRect( min.x, min.y, max.x, max.y );
            
            // ダブルクリック
            if( touch.isDoubleClick )
            {
                Vector2 pos = touch.pos;
                if( pos.x > min.x && pos.y > min.y && pos.x < max.x && pos.y < max.y )
                {
                    return true;
                }
            }
            // タッチ離されたか
            else if( touch.isRelease )
            {
                Vector2 pos = touch.pos;
                if( pos.x > min.x && pos.y > min.y && pos.x < max.x && pos.y < max.y )
                {
                    if( m_Root.isOpen == false )
                    {
                        if( touch.holdTime > 1.0f )
                        {
                            return true;
                        }
                    }
                }
            }
            
#if UNITY_EDITOR
            if( Input.GetKeyDown( KeyCode.F1 ) )
            {
                return true;
            }
#endif
            
            return false;
        }
        
        #endregion
        
        //=========================================================================
        //. 更新
        //=========================================================================
        #region 更新
        
        /// ***********************************************************************
        /// <summary>
        /// アイテムの更新
        /// </summary>
        /// ***********************************************************************
        private void RefreshItem( UnityEngine.SceneManagement.Scene scene )
        {
            m_SceneType = SceneType.NONE;
            
            if( m_Root != null )
            {
                RemoveItem( );
                
                if( scene.IsValid() )
                {
                    switch( scene.name )
                    {
                    case "root":
                        m_SceneType = SceneType.ROOT;
                        AddItem_Root( m_Root.AddItem( new ItemButton( ItemType.TITLE, "ルート", false ) ) );
                        break;
                    case "title":
                        m_SceneType = SceneType.TITLE;
                        AddItem_Title( m_Root.AddItem( new ItemButton( ItemType.TITLE, "タイトル", false ) ) );
                        break;
                    case "home":
                        m_SceneType = SceneType.HOME;
                        AddItem_Home( m_Root.AddItem( new ItemButton( ItemType.HOME, "ホーム", false ) ) );
                        break;
                    case "battle":
                    case "test_battle":
                        m_SceneType = SceneType.BATTLE;
                        AddItem_Battle( m_Root.AddItem( new ItemButton( ItemType.BATTLE, "バトル", false ) ) );
                        break;
                    }
                }
            }
        }
        
        /// ***********************************************************************
        /// <summary>
        /// アイテムの削除
        /// </summary>
        /// ***********************************************************************
        private void RemoveItem( )
        {
            if( m_Root != null )
            {
                m_Root.RemoveItem( FindItem( ItemType.ROOT ) );
                m_Root.RemoveItem( FindItem( ItemType.TITLE ) );
                m_Root.RemoveItem( FindItem( ItemType.HOME ) );
                m_Root.RemoveItem( FindItem( ItemType.BATTLE ) );
            }
        }
        
        #endregion
        
        //=========================================================================
        //. システムアイテム
        //=========================================================================
        #region システムアイテム
        
        /// ***********************************************************************
        /// <summary>
        /// システムアイテムの追加
        /// </summary>
        /// ***********************************************************************
        private void AddItem_System( ItemBase root )
        {
            root.AddItem( new ItemButton( ItemType.FPSDISP, "FPS表示", SystemManager.Instance.isFpsDisp, (ItemBase item) =>
                {
                    SystemManager.Instance.isFpsDisp = !SystemManager.Instance.isFpsDisp;
                    item.sw = SystemManager.Instance.isFpsDisp;
                    return false; // デバッグメニュー閉じない
                }
            ) );
            
            // ----------------------------------------
            // システム
            // ----------------------------------------
            {
                ItemBase system = root.AddItem( new ItemButton( ItemType.SYSTEM, "システム", false ) );
                
                system.AddItem( new ItemWindow<WindowDeviceInfo>( ItemType.DEVICE_INFO, "デバイス情報" ) );
                
                system.AddItem( new ItemButton( ItemType.LOG, "ログ", false, (ItemBase item) =>
                    {
                        m_Log.SetDisp( true );
                        return true; // デバッグメニュー閉じる
                    }
                ) );
                
                system.AddItem( new ItemWindow<WindowSceneHierarchy>( ItemType.HIERARCHY, "ヒエラルキー" ) );
                
                system.AddItem( new ItemWindow<WindowViewHistory>( ItemType.VIEW, "ビュー" ) );
                
                system.AddItem( new ItemWindow<WindowAsset>( ItemType.ASSET, "アセット" ) );
                
                // ----------------------------------------
                // デバッグ
                // ----------------------------------------
                {
                    ItemBase debug = system.AddItem( new ItemButton( ItemType.DEBUG, "デバッグ", false ) );
                    
                    // ----------------------------------------
                    // プロファイラ表示
                    // ----------------------------------------
                    debug.AddItem( new ItemWindow<DebugProfilerGUI>( ItemType.DEBUG_PROFILER, "プロファイラ" ) );
                    
                    // ----------------------------------------
                    // LocalizedText
                    // ----------------------------------------
                    // debug.AddItem( new ItemWindow<DebugLocalizedTextGUI>( ItemType.DEBUG_LOCALIZED_TEXT, "ローカライズテキスト" ) );
                    
                    // ----------------------------------------
                    // 雑用
                    // ----------------------------------------
                    //debug.AddItem(new ItemButton(ItemType.TEMP_01, "マスタリロード(タスク)", false, (ItemBase item) =>
                    //{
                    //    LWARS.MasterParam.DestroySingleton( );
                    //    StartCoroutine( new TaskWaitFor( LWARS.MasterParam.Instance.LoadNew( false ) ) );
                    //    return false; // デバッグメニュー閉じない
                    //}
                    //));
                    //debug.AddItem( new ItemButton( ItemType.TEMP_02, "マスタリロード(旧", false, ( ItemBase item ) =>
                    //{
                    //    LWARS.MasterParam.DestroySingleton( );
                    //    StartCoroutine( LWARS.MasterParam.Instance.Load( false ) );
                    //    return false; // デバッグメニュー閉じない
                    //}
                    //) );
                    //debug.AddItem(new ItemButton(ItemType.DUMMY_03, "ファイガ", false, (ItemBase item) =>
                    //{
                    //    // Scene.StartExecuter( Task_LoadCheck( "Unit/Lapis/JOB/BLM/ANIM/BLM_skl_fire_02_MM.asset" ) );
                    //    Scene.StartExecuter( Task_EffectLoad( "Effects/Lapis/Magic/b/ef_mb_fire_02.asset" ) );
                    //    return false; // デバッグメニュー閉じない
                    //}
                    //));
                }
                
                // ----------------------------------------
                // バグ報告
                // ----------------------------------------
                system.AddItem( new ItemWindow<WindowBugSend>( ItemType.BUGSEND, "バグ報告" ) );
            }

            AddItem_System_WW(root);
        }
        
        #endregion
        
        //=========================================================================
        //. ルートアイテム
        //=========================================================================
        #region ルートアイテム
        
        /// ***********************************************************************
        /// <summary>
        /// ルートアイテムの追加
        /// </summary>
        /// ***********************************************************************
        private void AddItem_Root( ItemBase root )
        {
        }
        
        #endregion
        
        //=========================================================================
        //. タイトルアイテム
        //=========================================================================
        #region タイトルアイテム
        
        /// ***********************************************************************
        /// <summary>
        /// タイトルアイテムの追加
        /// </summary>
        /// ***********************************************************************
        private void AddItem_Title( ItemBase root )
        {
        }
        
        #endregion
        
        //=========================================================================
        //. ホームアイテム
        //=========================================================================
        #region ホームアイテム
        
        /// ***********************************************************************
        /// <summary>
        /// ホームアイテムの追加
        /// </summary>
        /// ***********************************************************************
        private void AddItem_Home( ItemBase root )
        {
            root.AddItem( new ItemButton( ItemType.HOME_TESTMOVIE, "テスト動画", false, ( ItemBase item ) =>
            {
                SerializeValueList valueList = new SerializeValueList();
                valueList.AddField( "type", ( int )LWARS.MovieViewBase.Type.Beast );
                valueList.AddField( "path", "bahamut_test" );
                valueList.AddField( "volume", 1.0f );
                valueList.AddField( "fade", true );
                valueList.AddField( "skip", true );//m_IsFirstPlay == false );
                ViewManager.Push( SceneManager.Current, ViewLayer.Id.MOVIE, ViewManager.Type.MOVIE_CRI, valueList );
                return true; // デバッグメニュー閉じる
            } ) );
            
            ItemBase arena = root.AddItem( new ItemButton( ItemType.HOME_ARENA, "アリーナ", false ) );
            {
                arena.AddItem( new ItemButton( ItemType.HOME_ARENA_BP0, "BP 0", false, ( ItemBase item ) =>
                {
                    LWARS.WebAPI.Json_AP json = new LWARS.WebAPI.Json_AP();
                    {
                        json.val = 0;
                        json.at  = GFSYS.WebAPI.WebAPIManager.Instance.GetCurrUnixSec();
                        
                        LWARS.PlayerData.Instance.DeserializeArenaBp( json );
                    }
                    return false; // デバッグメニュー閉じない
                }
                ) );
                
                arena.AddItem( new ItemButton( ItemType.HOME_ARENA_BPFULL, "BP FULL", false, ( ItemBase item ) =>
                {
                    LWARS.WebAPI.Json_AP json = new LWARS.WebAPI.Json_AP();
                    {
                        json.val = LWARS.MasterParam.Instance.FixParam.ArenaBpCapacity;
                        json.at  = 10000;
                        
                        LWARS.PlayerData.Instance.DeserializeArenaBp( json );
                    }
                    return false; // デバッグメニュー閉じない
                }
                ) );
            }
            
            ItemBase guildBtl = root.AddItem( new ItemButton( ItemType.HOME_GUILDBTL, "ギルドバトル", false ) );
            {
                guildBtl.AddItem( new ItemButton( ItemType.HOME_GUILDBTL_RESULT_CHECK, "リザルト確認", false, ( ItemBase item ) =>
                {
                    {
                        LWARS.WebAPI.GuildBattleMatchEnd.Response res = new LWARS.WebAPI.GuildBattleMatchEnd.Response();
                        string txt = "{\"previous_statuses\":[null,null,null],\"current_statuses\":[{\"id\":190,\"hp\":0,\"mp\":62,\"ap\":22,\"abilities\":[]},{\"id\":191,\"hp\":0,\"mp\":62,\"ap\":58,\"abilities\":[]},{\"id\":192,\"hp\":0,\"mp\":52,\"ap\":16,\"abilities\":[]}],\"units\":[{\"id\":1586,\"exp\":0,\"fav\":0,\"jobs\":[{\"id\":1586,\"lv\":1,\"job_iname\":\"JB_LW_GUN\"}],\"rank\":0,\"awake\":1,\"brave\":50,\"faith\":50,\"iname\":\"UN_LW_P_FDRC\",\"jobpt\":9999,\"artset\":{},\"abilset\":{\"sub\":1586,\"sup1\":null,\"sup2\":null,\"react\":null},\"abilities\":[{\"id\":14029,\"lv\":0,\"type\":3,\"job_id\":0,\"ability_iname\":\"SK_LB_LW_FDRC\"},{\"id\":14028,\"lv\":1,\"type\":1,\"job_id\":1586,\"ability_iname\":\"SK_LW_GUN_M_1\"}],\"abilityboards\":[1]},{\"id\":1588,\"exp\":0,\"fav\":0,\"jobs\":[{\"id\":1588,\"lv\":1,\"job_iname\":\"JB_LW_LAN\"}],\"rank\":0,\"awake\":1,\"brave\":50,\"faith\":50,\"iname\":\"UN_LW_P_AILN\",\"jobpt\":9999,\"artset\":{},\"abilset\":{\"sub\":1588,\"sup1\":null,\"sup2\":null,\"react\":null},\"abilities\":[{\"id\":14033,\"lv\":0,\"type\":3,\"job_id\":0,\"ability_iname\":\"SK_LB_LW_AILN\"},{\"id\":14032,\"lv\":1,\"type\":1,\"job_id\":1588,\"ability_iname\":\"SK_LW_LAN_M_1\"}],\"abilityboards\":[1]},{\"id\":1592,\"exp\":0,\"fav\":0,\"jobs\":[{\"id\":1592,\"lv\":1,\"job_iname\":\"JB_LW_KNT\"}],\"rank\":0,\"awake\":1,\"brave\":50,\"faith\":50,\"iname\":\"UN_LW_P_LILS\",\"jobpt\":9999,\"artset\":{},\"abilset\":{\"sub\":1592,\"sup1\":null,\"sup2\":null,\"react\":null},\"abilities\":[{\"id\":14039,\"lv\":1,\"type\":1,\"job_id\":1592,\"ability_iname\":\"SK_LW_KNT_M_1\"}],\"abilityboards\":[1]}],\"star\":2,\"reward_iname\":\"PR_2\",\"player\":{\"experience\":5857307,\"items\":[{\"id\":15479,\"iname\":\"IT_EC_GUILD\",\"num\":518200},{\"id\":15533,\"iname\":\"IT_SE_SNAKE_S\",\"num\":1004220}]}}";
                        res = JsonUtility.FromJson<LWARS.WebAPI.GuildBattleMatchEnd.Response>( txt );
                        
                        LWARS.GuildBattleBattleEnd guildBattleEnd = new LWARS.GuildBattleBattleEnd();
                        guildBattleEnd.Deserialize( res );
                        
                        LWARS.PlayerData.Instance.SetGuildBattleBattleEnd( guildBattleEnd );
                    }
                    
                    SerializeValueList valueList = new SerializeValueList();
                    {
                        valueList.AddField( "isDebug", true );
                    }
                    
                    ViewManager.Push( ViewLayer.Id.GLOBALMENU, ViewManager.Type.GUILDBATTLE_RESULT, valueList );
                    
                    return false; // デバッグメニュー閉じない
                }
                ) );
                
                guildBtl.AddItem( new ItemButton( ItemType.HOME_GUILDBTL_RESULT_CHECK2, "リザルト確認(死なない)", false, ( ItemBase item ) =>
                {
                    {
                        LWARS.WebAPI.GuildBattleMatchEnd.Response res = new LWARS.WebAPI.GuildBattleMatchEnd.Response();
                        string txt = "{\"previous_statuses\":[null,null,null],\"current_statuses\":[{\"id\":190,\"hp\":0,\"mp\":62,\"ap\":22,\"abilities\":[]},{\"id\":191,\"hp\":0,\"mp\":62,\"ap\":58,\"abilities\":[]},{\"id\":192,\"hp\":0,\"mp\":52,\"ap\":16,\"abilities\":[]}],\"units\":[{\"id\":1586,\"exp\":0,\"fav\":0,\"jobs\":[{\"id\":1586,\"lv\":1,\"job_iname\":\"JB_LW_GUN\"}],\"rank\":0,\"awake\":1,\"brave\":50,\"faith\":50,\"iname\":\"UN_LW_P_FDRC\",\"jobpt\":9999,\"artset\":{},\"abilset\":{\"sub\":1586,\"sup1\":null,\"sup2\":null,\"react\":null},\"abilities\":[{\"id\":14029,\"lv\":0,\"type\":3,\"job_id\":0,\"ability_iname\":\"SK_LB_LW_FDRC\"},{\"id\":14028,\"lv\":1,\"type\":1,\"job_id\":1586,\"ability_iname\":\"SK_LW_GUN_M_1\"}],\"abilityboards\":[1]},{\"id\":1588,\"exp\":0,\"fav\":0,\"jobs\":[{\"id\":1588,\"lv\":1,\"job_iname\":\"JB_LW_LAN\"}],\"rank\":0,\"awake\":1,\"brave\":50,\"faith\":50,\"iname\":\"UN_LW_P_AILN\",\"jobpt\":9999,\"artset\":{},\"abilset\":{\"sub\":1588,\"sup1\":null,\"sup2\":null,\"react\":null},\"abilities\":[{\"id\":14033,\"lv\":0,\"type\":3,\"job_id\":0,\"ability_iname\":\"SK_LB_LW_AILN\"},{\"id\":14032,\"lv\":1,\"type\":1,\"job_id\":1588,\"ability_iname\":\"SK_LW_LAN_M_1\"}],\"abilityboards\":[1]},{\"id\":1592,\"exp\":0,\"fav\":0,\"jobs\":[{\"id\":1592,\"lv\":1,\"job_iname\":\"JB_LW_KNT\"}],\"rank\":0,\"awake\":1,\"brave\":50,\"faith\":50,\"iname\":\"UN_LW_P_LILS\",\"jobpt\":9999,\"artset\":{},\"abilset\":{\"sub\":1592,\"sup1\":null,\"sup2\":null,\"react\":null},\"abilities\":[{\"id\":14039,\"lv\":1,\"type\":1,\"job_id\":1592,\"ability_iname\":\"SK_LW_KNT_M_1\"}],\"abilityboards\":[1]}],\"star\":2,\"reward_iname\":\"PR_2\",\"player\":{\"experience\":5857307,\"items\":[{\"id\":15479,\"iname\":\"IT_EC_GUILD\",\"num\":518200},{\"id\":15533,\"iname\":\"IT_SE_SNAKE_S\",\"num\":1004220}]}}";
                        res = JsonUtility.FromJson<LWARS.WebAPI.GuildBattleMatchEnd.Response>( txt );
                        
                        LWARS.GuildBattleBattleEnd guildBattleEnd = new LWARS.GuildBattleBattleEnd();
                        guildBattleEnd.Deserialize( res );
                        
                        LWARS.PlayerData.Instance.SetGuildBattleBattleEnd( guildBattleEnd );
                    }
                    
                    SerializeValueList valueList = new SerializeValueList();
                    {
                        valueList.AddField( "isDebug", true );
                        valueList.AddField( "dbgType", (int)1 );
                    }
                    
                    ViewManager.Push( ViewLayer.Id.GLOBALMENU, ViewManager.Type.GUILDBATTLE_RESULT, valueList );
                    
                    return false; // デバッグメニュー閉じない
                }
                ) );
            }

            ItemBase classMatch = root.AddItem(new ItemButton(ItemType.HOME_CLASS_MATCH, "クラスマッチ", false));
            {
                classMatch.AddItem(new ItemButton(ItemType.HOME_CLASS_MATCH_VS_NPC_ON, "NPC戦ON", false, (ItemBase item) =>
                {
                    LWARS.PlayerData.Instance.SetClassMatchVsNpcFlag(true);
                    
                    return true; // デバッグメニュー閉じる
                }
                ));
                classMatch.AddItem(new ItemButton(ItemType.HOME_CLASS_MATCH_VS_NPC_OFF, "NPC戦OFF", false, (ItemBase item) =>
                {
                    LWARS.PlayerData.Instance.SetClassMatchVsNpcFlag(false);

                    return true; // デバッグメニュー閉じる
                }
                ));
            }

            ItemBase seCheck = root.AddItem(new ItemButton(ItemType.HOME_SE, "SEチェック", false));
            {
                string seKey = "Motion.se_fs_armor_walk";
                seCheck.AddItem(new ItemButton(ItemType.HOME_SE_TEST01, seKey, false, (ItemBase item) =>
                {
                    LWARS.WebAPI.Json_AP json = new LWARS.WebAPI.Json_AP();
                    {
                        SoundManager.Instance.PlaySEOneShot(seKey);
                    }
                    return false; // デバッグメニュー閉じない
                }
                ));
            }

            ItemBase mission = root.AddItem(new ItemButton(ItemType.HOME_MISSION, "ミッション", false));
            {
                mission.AddItem(new ItemButton(ItemType.HOME_MISSION_PACK01, "パック購入01", false, (ItemBase item) =>
                {
                    string iname = "MP_TEST_01";
                    if (LWARS.PlayerData.Instance.GetMissionPack(iname) == null)
                    {
                        LWARS.PlayerData.Instance.SetPurchasedMissionPack(iname);
                    }
                    return false; // デバッグメニュー閉じない
                }
                ));

                mission.AddItem(new ItemButton(ItemType.HOME_MISSION_PACK02, "パック購入02", false, (ItemBase item) =>
                {
                    string iname = "MP_TEST_02";
                    if (LWARS.PlayerData.Instance.GetMissionPack(iname) == null)
                    {
                        LWARS.PlayerData.Instance.SetPurchasedMissionPack(iname);
                    }
                    return false; // デバッグメニュー閉じない
                }
                ));
            }

            ItemBase other = root.AddItem(new ItemButton(ItemType.HOME_OTHER, "その他", false));
            {
                other.AddItem(new ItemButton(ItemType.HOME_OTHER_VIPRANK, "VIP", false, (ItemBase item) =>
                {
                    DebugUtility.LogWarning(LWARS.MasterParam.Instance.GetPowSpeedType().ToString());
                    return false; // デバッグメニュー閉じない
                }
                ));
                
                other.AddItem(new ItemButton(ItemType.HOME_OTHER_CAPOVER, "アイテム超過", false, (ItemBase item) =>
                {
                    LWARS.WebAPI.RewardItem rewardItem = new LWARS.WebAPI.RewardItem();
                    {
                        LWARS.WebAPI.Json_CapOver capOver = new LWARS.WebAPI.Json_CapOver();
                        {
                            capOver.artifacts = new string[]
                            {
                                "AF_KAT_LW_ROBB_2",
                                "AF_LW_SPE_002",
                                "AF_LW_BSW_000",
                            };
                            
                            capOver.visioncards = new string[]
                            {
                                "VC_LW_RAMU",
                                "VC_LW_SIRE",
                            };
                        }
                        rewardItem.cap_over = capOver;
                    };
                    
                    LWARS.PlayerData.Instance.SetRewardItemData( rewardItem );
                    return false; // デバッグメニュー閉じない
                }
                ));
                
                other.AddItem(new ItemButton(ItemType.HOME_OTHER_DEBUG00, "HomeBg(ON/OFF)", true, (ItemBase item) =>
                {
                    GameObject gobj = item.valueList.GetGameObject( "Value" );
                    if( gobj != null )
                    {
                        gobj.SetActive( !gobj.activeSelf );
                    }
                    else
                    {
                        ViewBase view = ViewController.GetView( ViewManager.Type.HOME_BG );
                        if( view != null )
                        {
                            gobj = view.ValueList.GetGameObject( "home_BG" );
                            if( gobj != null )
                            {
                                gobj.SetActive( !gobj.activeSelf );
                                item.valueList.SetField( "Value", gobj );
                            }
                        }
                    }
                    return false; // デバッグメニュー閉じない
                }
                )).CreateValueList( null ).
                SetPreProcess( item =>
                {
                    GameObject gobj = item.valueList.GetGameObject( "Value" );
                    if( gobj != null )
                    {
                        item.sw = gobj.activeSelf;
                    }
                    return true;
                } );
                
                other.AddItem(new ItemButton(ItemType.HOME_OTHER_DEBUG01, "CharaRoot(ON/OFF)", true, (ItemBase item) =>
                {
                    GameObject gobj = item.valueList.GetGameObject( "Value" );
                    if( gobj != null )
                    {
                        gobj.SetActive( !gobj.activeSelf );
                    }
                    else
                    {
                        ViewBase view = ViewController.GetView( ViewManager.Type.HOME_BG );
                        if( view != null )
                        {
                            gobj = view.ValueList.GetGameObject( "CharaRoot" );
                            if( gobj != null )
                            {
                                gobj.SetActive( !gobj.activeSelf );
                                item.valueList.SetField( "Value", gobj );
                            }
                        }
                    }
                    return false; // デバッグメニュー閉じない
                }
                )).CreateValueList( null ).
                SetPreProcess( item =>
                {
                    GameObject gobj = item.valueList.GetGameObject( "Value" );
                    if( gobj != null )
                    {
                        item.sw = gobj.activeSelf;
                    }
                    return true;
                } );
                
                other.AddItem(new ItemButton(ItemType.HOME_OTHER_DEBUG02, "CharaBodyRoot(ON/OFF)", true, (ItemBase item) =>
                {
                    GameObject gobj = item.valueList.GetGameObject( "Value" );
                    if( gobj != null )
                    {
                        gobj.SetActive( !gobj.activeSelf );
                    }
                    else
                    {
                        ViewBase view = ViewController.GetView( ViewManager.Type.HOME_BG );
                        if( view != null )
                        {
                            gobj = view.ValueList.GetGameObject( "CharaBodyRoot" );
                            if( gobj != null )
                            {
                                gobj.SetActive( !gobj.activeSelf );
                                item.valueList.SetField( "Value", gobj );
                            }
                        }
                    }
                    return false; // デバッグメニュー閉じない
                }
                )).CreateValueList( null ).
                SetPreProcess( item =>
                {
                    GameObject gobj = item.valueList.GetGameObject( "Value" );
                    if( gobj != null )
                    {
                        item.sw = gobj.activeSelf;
                    }
                    return true;
                } );
                
                other.AddItem(new ItemButton(ItemType.HOME_OTHER_DEBUG03, "LightAll(ON/OFF)", true, (ItemBase item) =>
                {
                    GameObject gobj = item.valueList.GetGameObject( "Value" );
                    if( gobj != null )
                    {
                        gobj.SetActive( !gobj.activeSelf );
                    }
                    else
                    {
                        ViewBase view = ViewController.GetView( ViewManager.Type.HOME_BG );
                        if( view != null )
                        {
                            gobj = view.ValueList.GetGameObject( "LightAll" );
                            if( gobj != null )
                            {
                                gobj.SetActive( !gobj.activeSelf );
                                item.valueList.SetField( "Value", gobj );
                            }
                        }
                    }
                    return false; // デバッグメニュー閉じない
                }
                )).CreateValueList( null ).
                SetPreProcess( item =>
                {
                    GameObject gobj = item.valueList.GetGameObject( "Value" );
                    if( gobj != null )
                    {
                        item.sw = gobj.activeSelf;
                    }
                    return true;
                } );
                
                other.AddItem(new ItemButton(ItemType.HOME_OTHER_DEBUG04, "Effect(ON/OFF)", true, (ItemBase item) =>
                {
                    GameObject gobj = item.valueList.GetGameObject( "Value" );
                    if( gobj != null )
                    {
                        gobj.SetActive( !gobj.activeSelf );
                    }
                    else
                    {
                        ViewBase view = ViewController.GetView( ViewManager.Type.HOME_BG );
                        if( view != null )
                        {
                            gobj = view.ValueList.GetGameObject( "home_BG" );
                            if( gobj != null )
                            {
                                GameObject child = gobj.FindChild( "effect" );
                                if( child != null )
                                {
                                    child.SetActive( !child.activeSelf );
                                    item.valueList.SetField( "Value", child );
                                }
                            }
                        }
                    }
                    return false; // デバッグメニュー閉じない
                }
                )).CreateValueList( null ).
                SetPreProcess( item =>
                {
                    GameObject gobj = item.valueList.GetGameObject( "Value" );
                    if( gobj != null )
                    {
                        item.sw = gobj.activeSelf;
                    }
                    return true;
                } );
                
                other.AddItem(new ItemButton(ItemType.HOME_OTHER_DEBUG05, "BG_2D(ON/OFF)", true, (ItemBase item) =>
                {
                    GameObject gobj = item.valueList.GetGameObject( "Value" );
                    if( gobj != null )
                    {
                        gobj.SetActive( !gobj.activeSelf );
                    }
                    else
                    {
                        ViewBase view = ViewController.GetView( ViewManager.Type.HOME_BG );
                        if( view != null )
                        {
                            gobj = view.ValueList.GetGameObject( "home_BG" );
                            if( gobj != null )
                            {
                                GameObject child = gobj.FindChild( "BG_2D" );
                                if( child != null )
                                {
                                    child.SetActive( !child.activeSelf );
                                    item.valueList.SetField( "Value", child );
                                }
                            }
                        }
                    }
                    return false; // デバッグメニュー閉じない
                }
                )).CreateValueList( null ).
                SetPreProcess( item =>
                {
                    GameObject gobj = item.valueList.GetGameObject( "Value" );
                    if( gobj != null )
                    {
                        item.sw = gobj.activeSelf;
                    }
                    return true;
                } );
                
                other.AddItem(new ItemButton(ItemType.HOME_OTHER_DEBUG06, "BG_3D(ON/OFF)", true, (ItemBase item) =>
                {
                    GameObject gobj = item.valueList.GetGameObject( "Value" );
                    if( gobj != null )
                    {
                        gobj.SetActive( !gobj.activeSelf );
                    }
                    else
                    {
                        ViewBase view = ViewController.GetView( ViewManager.Type.HOME_BG );
                        if( view != null )
                        {
                            gobj = view.ValueList.GetGameObject( "home_BG" );
                            if( gobj != null )
                            {
                                GameObject child = gobj.FindChild( "BG_3D" );
                                if( child != null )
                                {
                                    child.SetActive( !child.activeSelf );
                                    item.valueList.SetField( "Value", child );
                                }
                            }
                        }
                    }
                    return false; // デバッグメニュー閉じない
                }
                )).CreateValueList( null ).
                SetPreProcess( item =>
                {
                    GameObject gobj = item.valueList.GetGameObject( "Value" );
                    if( gobj != null )
                    {
                        item.sw = gobj.activeSelf;
                    }
                    return true;
                } );
            }
        }
        
        #endregion
        
        //=========================================================================
        //. バトルアイテム
        //=========================================================================
        #region バトルアイテム
        
        /// ***********************************************************************
        /// <summary>
        /// バトルアイテムの追加
        /// </summary>
        /// ***********************************************************************
        private void AddItem_Battle( ItemBase root )
        {
            // ----------------------------------------
            // バトルデバッグ
            // ----------------------------------------
            root.AddItem( new ItemWindow<WindowBattleDebug>( ItemType.BATTLE_DEBUG, "バトルデバッグ" ) );
            
            // ----------------------------------------
            // 描画
            // ----------------------------------------
            {
                ItemBase draw = root.AddItem( new ItemButton( ItemType.BATTLE_DRAW, "描画", false ) );
                
                draw.AddItem(new ItemButton(ItemType.BATTLE_DRAW_BLUR, "ブラーON/OFF", false, (ItemBase item) =>
                {
                    LWARS.RenderPipeline pipeline = null;
                    if( Camera.main != null ) pipeline = Camera.main.gameObject.GetComponent<LWARS.RenderPipeline>( );
                    if( pipeline != null )
                    {
                        item.sw = !item.sw;
                        if( item.sw ) pipeline.ScreenBlurOn( 0.25f, 0.9f );
                        else          pipeline.ScreenBlurOff( 0.5f );
                    }
                    return false; // デバッグメニュー閉じない
                }
                ));
                
                draw.AddItem(new ItemButton(ItemType.BATTLE_DRAW_SNAP, "スナップON/OFF", false, (ItemBase item) =>
                {
                    LWARS.RenderPipeline pipeline = null;
                    if( Camera.main != null ) pipeline = Camera.main.gameObject.GetComponent<LWARS.RenderPipeline>( );
                    if( pipeline != null )
                    {
                        item.sw = !item.sw;
                        if( item.sw ) pipeline.ScreenSnapOn( 0.25f );
                        else          pipeline.ScreenSnapOff( 0.5f );
                    }
                    return false; // デバッグメニュー閉じない
                }
                ));
                
                draw.AddItem(new ItemButton( ItemType.BATTLE_CHANGE_TRANSSHADER , "半透明シェーダー", false, (ItemBase item) =>
                    {
                        item.sw = !item.sw;
                        if( item.sw )
                        {
                            List<LWARS.TacticsUnitController> list = LWARS.BattleMain.Instance.TacticsUnits;
                            for( int i = 0; i < list.Count; ++i )
                            {
                                list[i].ChangeShader( LWARS.UnitController.EShaderType.Transparent );
                                list[i].SetOpacity( 0.5f );
                            }
                        }
                        else
                        {
                            List<LWARS.TacticsUnitController> list = LWARS.BattleMain.Instance.TacticsUnits;
                            for( int i = 0; i < list.Count; ++i )
                            {
                                list[i].ChangeShader( LWARS.UnitController.EShaderType.Default );
                            }
                        }
                        return false; // デバッグメニュー閉じない
                    }
                ));
                
                string[] camera_mode = System.Enum.GetNames( typeof( LWARS.RenderPipeline.ERenderMode ) );
                draw.AddItem(new ItemSelector(ItemType.BATTLE_DRAW_CAMERAMODE, "カメラモード", camera_mode, 0, (ItemBase item) =>
                {
                    LWARS.RenderPipeline pipeline = null;
                    if( Camera.main != null ) pipeline = Camera.main.gameObject.GetComponent<LWARS.RenderPipeline>( );
                    if( pipeline != null ) pipeline.RenderMode = (LWARS.RenderPipeline.ERenderMode)item.select;
                    return false; // デバッグメニュー閉じない
                }
                ));
                
                string[] shadowcam = new string[] { "[Near:5,Far:100,Dist:50]", "Near+0.1", "Near-0.1", "Far+1", "Far-1", "Dist+1", "Dist-1" };
                draw.AddItem(new ItemSelector(ItemType.BATTLE_DRAW_SHADOWCAM, "影カメラ調整", shadowcam, 0, true, (ItemBase item) =>
                {
                    LWARS.RenderPipeline pipeline = null;
                    if( Camera.main != null ) pipeline = Camera.main.gameObject.GetComponent<LWARS.RenderPipeline>( );
                    if( pipeline != null )
                    {
                        if( item.select == 0 )
                        {
                            pipeline.ShadowNear = 5;
                            pipeline.ShadowFar = 100;
                            pipeline.ShadowDist = 50;
                        }
                        else if( item.select == 3 ) pipeline.ShadowNear += 0.1f;
                        else if( item.select == 4 ) pipeline.ShadowNear -= 0.1f;
                        else if( item.select == 5 ) pipeline.ShadowFar += 1f;
                        else if( item.select == 6 ) pipeline.ShadowFar -= 1f;
                        else if( item.select == 7 ) pipeline.ShadowDist += 1f;
                        else if( item.select == 8 ) pipeline.ShadowDist -= 1f;
                        string[] tbl = item.tbl;
                        tbl[0] = "[Near:" + pipeline.ShadowNear + ",Far:" + pipeline.ShadowFar + ",Dist:" + pipeline.ShadowDist.ToString( ) + "]";
                    }
                    return false; // デバッグメニュー閉じない
                }
                ));
            }
            
            // ----------------------------------------
            // フラグ
            // ----------------------------------------
            {
                ItemBase flag = root.AddItem( new ItemButton( ItemType.BATTLE_SETTING, "設定", false ) );
                
                flag.AddItem( new ItemSelector( ItemType.BATTLE_FLAG_SPEED, "倍速", new string[] { "1倍速", "1.5倍速", "2倍速", "2.5倍速", "3倍速" }, 0, (ItemBase item) =>
                    {
                        if( item.select == 0 )      LWARS.GlobalVars.BattleSpeed = 1.0f;
                        else if( item.select == 1 ) LWARS.GlobalVars.BattleSpeed = 1.5f;
                        else if( item.select == 2 ) LWARS.GlobalVars.BattleSpeed = 2.0f;
                        else if( item.select == 3 ) LWARS.GlobalVars.BattleSpeed = 2.5f;
                        else if( item.select == 4 ) LWARS.GlobalVars.BattleSpeed = 3.0f;
                        return false; // デバッグメニュー閉じない
                    }
                ) ).SetPreProcess( (ItemBase item) =>
                {
                    if( LWARS.GlobalVars.BattleSpeed == 1.5f )      item.select = 1;
                    else if( LWARS.GlobalVars.BattleSpeed == 2.0f ) item.select = 2;
                    else if( LWARS.GlobalVars.BattleSpeed == 2.5f ) item.select = 3;
                    else if( LWARS.GlobalVars.BattleSpeed == 3.0f ) item.select = 4;
                    else                                             item.select = 0;
                    return true;
                } );
                
                // グリッド
                //flag.AddItem( new ItemButton( ItemType.BATTLE_CHANGE_GRID, "グリッド変更", LWARS.DebugValue.IsUnlimitedMove, (ItemBase item) =>
                //    {
                //        //string key = LWARS.BattleMain.Instance.MapView.CurrentGridKey;
                //        //var dataTbl = LWARS.BattleMain.Instance.MapView.GridDatas;
                //        //if( dataTbl != null && dataTbl.Length > 1 )
                //        //{
                //        //    for( int i = 0, max = dataTbl.Length; i < max; ++i )
                //        //    {
                //        //        if( dataTbl[i].name == key )
                //        //        {
                //        //            int idx = ( i + 1 ) % dataTbl.Length;
                //        //            LWARS.BattleMain.Instance.MapView.ChangeGrid( dataTbl[idx].name );
                //        //            item.label = "グリッド変更【" + LWARS.BattleMain.Instance.MapView.CurrentGridKey + "】";
                //        //            break;
                //        //        }
                //        //    }
                //        //}
                //        return false; // デバッグメニュー閉じない
                //    }
                //) );
            }
            
            // ----------------------------------------
            // 確認
            // ----------------------------------------
            {
                ItemBase confirm = root.AddItem( new ItemButton( ItemType.BATTLE_CONFIRM, "演出確認", false ) );
                
                confirm.AddItem(new ItemButton(ItemType.BATTLE_CONFIRM_TALK, "会話チェック", false, (ItemBase item) =>
                        {
                            ViewManager.Push( ViewManager.Type.BATTLE_END );
                            
//                            LWARS.TalkView.Create( LWARS.TalkView.Mode.Chant, LWARS.TalkView.ViewPosition.Up, "UN_LW_P_MONT", "テストテストテストテストテストテストテストテストテストテストテストテストテストテストテストテストテストテストテストテストテストテストテストテストテストテストテストテストテストテストテストテストテストテストテストテストテストテストテストテストテストテストテストテストテストテストテストテストテストテストテスト" );
//                            LWARS.TalkView.CreateDemo(LWARS.TalkView.ViewPosition.Bottom, "DU_LW_ELDE", "JB_LW_WAR", "テストテストテストテストテストテストテストテストテストテストテストテストテストテストテストテストテストテストテストテストテストテストテストテストテストテストテストテストテストテストテストテストテストテストテストテストテストテストテストテストテストテストテストテストテストテストテストテストテストテストテスト" );
                            return false; // デバッグメニュー閉じない
                        }
                ));
                
                confirm.AddItem(new ItemButton(ItemType.BATTLE_CONFIRM_WIN, "強制勝利演出", false, (ItemBase item) =>
                        {
                            LWARS.BattleCore.Instance.StatusLog.SetForceDropItem( );
                            LWARS.BattleMain.Instance.SetForceExit( LWARS.EBattleResultType.Win );
                            return true; // デバッグメニュー閉じる
                        }
                ));
                
                confirm.AddItem(new ItemButton(ItemType.BATTLE_CONFIRM_LOSE, "強制敗北演出", false, (ItemBase item) =>
                        {
                            LWARS.BattleMain.Instance.SetForceExit( LWARS.EBattleResultType.GameOver );
                            return true; // デバッグメニュー閉じる
                        }
                ));
                
                confirm.AddItem(new ItemButton(ItemType.BATTLE_CONFIRM_CONTINUE, "コンティニュー", false, (ItemBase item) =>
                        {
                            if( LWARS.BattleMain.Instance.RootView.isSignal == false )
                            {
                                LWARS.BattleMain.Instance.OnEvent( LWARS.BattleMain.EEventType.Continue, null );
                                return true; // デバッグメニュー閉じる
                            }
                            return false; // デバッグメニュー閉じない
                        }
                ));
                
                confirm.AddItem(new ItemButton(ItemType.BATTLE_CONFIRM_SOUND, "サウンド差し替え", false, (ItemBase item) =>
                        {
                            if( SoundManager.HasInstance() )
                            {
                                SoundManager.ReCreate();
                                SoundManager.Instance.LoadSystemSE();
                            }
                            return false; // デバッグメニュー閉じない
                        }
                ));

                //confirm.AddItem(new ItemButton(ItemType.BATTLE_CONFIRM_SUMMON, "召喚演出", false, (ItemBase item) =>
                //        {
                //            LWARS.BattleMapView mapView = LWARS.BattleMain.Instance.MapView;
                //            if( mapView != null && mapView.IsEndChangeStageBg() )
                //            {
                //                if( mapView.ChangeBg == null || mapView.ChangeBg.isDisable )
                //                {
                //                    item.label = "召喚演出OFF";
                //                    mapView.ChangeBg( "EffectDarkField" );
                //                }
                //                else if( mapView.ChangeBg.isEnable )
                //                {
                //                    item.label = "召喚演出ON";
                //                    mapView.ResetStageBg();
                //                }
                //            }
                //            return false; // デバッグメニュー閉じない
                //        }
                //));
                
                {
                    ItemButton confirmSleep = new ItemButton(ItemType.BATTLE_CONFIRM_SLEEP, "Sleep: ", false, null );
                    confirmSleep.SetUpdateProcess( (ItemBase item) =>
                        {
                            string txt = "none";
                            if( Screen.sleepTimeout == SleepTimeout.NeverSleep )
                            {
                                txt = "NeverSleep";
                            }
                            else if( Screen.sleepTimeout == SleepTimeout.SystemSetting )
                            {
                                txt = "SystemSetting";
                            }
                        
                            item.label = "Sleep: " + txt;
                            return false; // デバッグメニュー閉じない
                        } );
                    confirm.AddItem( confirmSleep );
                }
            }
            
            // ----------------------------------------
            // ウィンド
            // ----------------------------------------
            {
                ItemBase window = root.AddItem( new ItemButton( ItemType.BATTLE_WINDOW, "ウィンド", false ) );
                
                // ----------------------------------------
                // 演出ログリスト
                // ----------------------------------------
                window.AddItem( new ItemWindow<WindowBattleLogList>( ItemType.BATTLE_WINDOW_LOGLIST, "演出ログリスト" ) );
                
                // ----------------------------------------
                // ユニットプレビュー
                // ----------------------------------------
                window.AddItem( new ItemWindow<WindowUnitPreview>( ItemType.BATTLE_WINDOW_UNITPREVIEW, "ユニットプレビュー" ) );
            }
        }
        
        #endregion
        
        //=========================================================================
        //. ヘルパー
        //=========================================================================
        
        /// ***********************************************************************
        /// <summary>
        /// ロードタスク
        /// </summary>
        /// ***********************************************************************
        private IEnumerator Task_LoadCheck( string path )
        {
            DebugUtility.Log( "DEBUG", "DebugLoad Begin > " + path );
            
            LoaderRequest loadReq = AssetManager.LoadAsync<LWARS.EventParam>( path );
            if( loadReq == null ) yield break;
            
            while( loadReq.isDone == false )
            {
                yield return null;
            }
            
            DebugUtility.Log( "DEBUG", "DebugLoad Done > " + path );
            
            loadReq.Unload( );
            
            DebugUtility.Log( "DEBUG", "DebugLoad Unload > " + path );
        }
        
        /// ***********************************************************************
        /// <summary>
        /// エフェクトロードタスク
        /// </summary>
        /// ***********************************************************************
        private IEnumerator Task_EffectLoad( string path )
        {
            DebugUtility.Log( "DEBUG", "DebugEffectLoad Begin > " + path );
            
            LoaderRequest loadReq = AssetManager.LoadAsync<LWARS.EffectParam>( path );
            if( loadReq == null ) yield break;
            
            while( loadReq.isDone == false )
            {
                yield return null;
            }
            
            DebugUtility.Log( "DEBUG", "DebugEffectLoad Done > " + path );
            
            DebugUtility.Log( "DEBUG", "DebugEffectLoad Unload > " + path );
            
            LWARS.EffectParam effectParam = loadReq.asset as LWARS.EffectParam;
            if( effectParam == null ) yield break;
            
            yield return null;
            yield return null;
            yield return null;
            
            Vector3 pos = new Vector3( 5.0f, 2.0f, 5.0f );
            LWARS.EffectData effect = effectParam.CreateEffect( pos, Quaternion.identity );
            
            loadReq.Unload( );
            
            if( effect != null )
            {
                DebugUtility.Log( "DEBUG", "DebugEffectLoad Play > " + path );
                effect.SetAliveTime( 3.0f );
                effect.Play( );
                
                while( effect.IsEnd == false )
                {
                    yield return null;
                }
                
                DebugUtility.Log( "DEBUG", "DebugEffectLoad PlayEnd > " + path );
            }
        }
        
        /// ***********************************************************************
        /// <summary>
        /// アセットバンドルロードタスク
        /// </summary>
        /// ***********************************************************************
        private IEnumerator Task_LoadAssetBundle( string manifestPath, string bundlePath )
        {
            DebugAssetBundleLoader loader = new DebugAssetBundleLoader( );
            
            yield return loader.LoadAsync( manifestPath, bundlePath );
            
            Object res = loader.GetObject( );
            if( res == null )
            {
                yield break;
            }
            
            loader.Unload( false );
            
            GameObject gobj = GameObject.Instantiate( res ) as GameObject;
            
            LWARS.DebugBattleRootView view = GameObject.FindObjectOfType<LWARS.DebugBattleRootView>( );
            gobj.SetParent( view.gameObject.transform, false ); 
        }
        
        //=========================================================================
        //. デバッグ用アセットバンドルローダー
        //=========================================================================
        public class DebugAssetBundleLoader
        {
            static string   LOADROOT_PATH = Application.streamingAssetsPath + "/AssetBundles/win/";
            
            private AssetBundleManifest m_Manifest      = null;
            private Object              m_Object        = null;
            private List<AssetBundle>   m_Bundles       = new List<AssetBundle>( );
            private List<string>        m_Dependencies  = new List<string>( );
            
            /// ***********************************************************************
            /// <summary>
            /// オブジェクトの取得
            /// </summary>
            /// ***********************************************************************
            public UnityEngine.Object GetObject( )
            {
                return m_Object;
            }
            
            /// ***********************************************************************
            /// <summary>
            /// オブジェクトの取得
            /// </summary>
            /// ***********************************************************************
            public T GetObject<T>( ) where T : UnityEngine.Object
            {
                return m_Object as T;
            }
            
            /// ***********************************************************************
            /// <summary>
            /// 依存ファイルのチェック
            /// </summary>
            /// ***********************************************************************
            private void CheckDependencies( string bundlePath )
            {
                //m_Dependencies.Add( "textmeshelement.unity3d" );
                ////m_Dependencies.Add( "default.unity3d" );
                //m_Dependencies.Add( "textmeshshader.unity3d" );
                //return;
                // 依存関係を走査して全ての依存ファイルをロード
                string[] deps = m_Manifest.GetAllDependencies( bundlePath );
                foreach( var dep in deps )
                {
                    if( m_Dependencies.FindIndex( prop => prop == dep ) != - 1 ) continue;
                    m_Dependencies.Add( dep );
                    CheckDependencies( dep );
                }
            }
            
            /// ***********************************************************************
            /// <summary>
            /// アンロード
            /// </summary>
            /// ***********************************************************************
            public void Unload( bool unloadAllLoadedObjects )
            {
                for( int i = 0; i < m_Bundles.Count; ++i )
                {
                    m_Bundles[i].Unload( unloadAllLoadedObjects );
                }
                m_Bundles.Clear( );
            }
            
            /// ***********************************************************************
            /// <summary>
            /// ロード
            /// </summary>
            /// ***********************************************************************
            public IEnumerator LoadAsync( string manifestPath, string bundlePath )
            {
                DebugUtility.Log( "Load Start {0}", bundlePath );
                
                // マニフェストのロード
                yield return LoadAsync_Manifest( manifestPath );
                
                // ファイルチェック
                if( m_Manifest == null )
                {
                    DebugUtility.Log( "Could not load object manifest {0}", manifestPath );
                    yield break;
                }
                
                // 依存ファイルをチェック
                CheckDependencies( bundlePath );
                
                // 依存ファイルをロード
                for( int i = 0; i < m_Dependencies.Count; ++i )
                {
                    yield return LoadAsync_AssetBundle( m_Dependencies[i] );
                }
                
                // アセットバンドルロード
                yield return LoadAsync_AssetBundle( bundlePath );
                
                // アセットバンドルを取得
                AssetBundle assetBundle = m_Bundles.Find( prop => prop.name == bundlePath );
                if( assetBundle == null )
                {
                    DebugUtility.Log( "Could not load asset bundle {0}", bundlePath );
                    yield break;
                }
                
                // オブジェクトをロード
                AssetBundleRequest op = assetBundle.LoadAssetAsync( System.IO.Path.GetFileNameWithoutExtension( bundlePath ) );
                yield return op;
                
                // オブジェクトを取得
                m_Object = op.asset;
                if( m_Object == null )
                {
                    DebugUtility.Log( "Could not load object {0}", System.IO.Path.GetFileNameWithoutExtension( bundlePath ) );
                    yield break;
                }
                
                DebugUtility.Log( "Load Done {0}", bundlePath );
            }
            
            /// ***********************************************************************
            /// <summary>
            /// マニフェストのロード
            /// </summary>
            /// ***********************************************************************
            public IEnumerator LoadAsync_Manifest( string manifestPath )
            {
                DebugUtility.Log( "Load Manifest Start {0}", manifestPath );
                
                // アセットバンドルロード
                AssetBundleCreateRequest assetBundleRequest = AssetBundle.LoadFromFileAsync( LOADROOT_PATH + manifestPath );
                yield return assetBundleRequest;
                
                // アセットバンドルの取得
                AssetBundle assetBundle = assetBundleRequest.assetBundle;
                if( assetBundle == null )
                {
                    DebugUtility.Log( "Could not load asset bundle manifest {0}", manifestPath );
                    yield break;
                }
                
                // マニフェストファイルロード
                AssetBundleRequest op = assetBundle.LoadAssetAsync<AssetBundleManifest>( "AssetBundleManifest" );
                yield return op;
                
                // オブジェクト取得
                m_Manifest = op.asset as AssetBundleManifest;
                
                // マニフェストのアセットバンドルはすぐに閉じる
                assetBundle.Unload( false );
                
                DebugUtility.Log( "Load Manifest Done {0}", manifestPath );
            }
            
            /// ***********************************************************************
            /// <summary>
            /// アセットバンドルのロード
            /// </summary>
            /// ***********************************************************************
            public IEnumerator LoadAsync_AssetBundle( string bundlePath )
            {
                // 既に読み込まれているファイルは無視
                if( m_Bundles.FindIndex( ( prop ) => prop.name == bundlePath ) != -1 )
                {
                    yield break;
                }
                
                DebugUtility.Log( "Load AssetBundle Start {0}", bundlePath );
                
                // アセットバンドルロード
                AssetBundleCreateRequest assetBundleRequest = AssetBundle.LoadFromFileAsync( LOADROOT_PATH + bundlePath );
                yield return assetBundleRequest;
                
                // アセットバンドルの取得
                AssetBundle assetBundle = assetBundleRequest.assetBundle;
                if( assetBundle == null )
                {
                    DebugUtility.Log( "Could not load asset bundle {0}", bundlePath );
                    yield break;
                }
                
                // 保存
                m_Bundles.Add( assetBundle );
                
                DebugUtility.Log( "Load AssetBundle Done {0}", bundlePath );
            }
        }
    }
#endif
}
