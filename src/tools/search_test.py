# -*- coding: utf-8 -*-
import shutil, os
import unittest

import search

class SearchTestCase(unittest.TestCase):
    # class "FAKE" constants
    # TEST_FOLDER_NAME = "unit_test"

    def setUp(self):
        print( "[SETUP]  SearchTestCase" )

        # # create a sub folder for testing
        # os.mkdir( self.TEST_FOLDER_NAME )
    # end of function; setUp

    def tearDown(self):
        print( "[TEARDOWN]  SearchTestCase" )

        # # remove the testing sub folder
        # shutil.rmtree( self.TEST_FOLDER_NAME )
    # end of function; tearDown

    ########################
    # UNIT TESTS
    ########################
    def test_GatherStringifyTextWithSingleDelimiter(self):
        print( "[TEST]  searchTestCase.test_GatherStringifyTextWithSingleDelimiter" )

        delimiter = '\"'
        test_case = "\"xxx\", 'abc', ~123~, !efg!"

        results = search.GatherStringifyTextWithSingleDelimiter( test_case, delimiter )
        print( "results: " )
        print( results )
    # end of function; test_GatherStringifyTextWithSingleDelimiter

    def test_GatherStringifyText(self):
        print( "[TEST]  searchTestCase.test_GatherStringifyText" )

        delimiterStart = '('
        delimiterEnd = ')'
        test_case = "{xxx}, [abc], <123>, (efg)"

        results = search.GatherStringifyText( test_case, delimiterStart, delimiterEnd )
        print( "results: " )
        print( results )
    # end of function; test_GatherStringifyText


    def test_SearchForNonEnglishTextInFile(self):
        print( "[TEST]  searchTestCase.test_SearchForNonEnglishTextInFile" )

        delimiter = '\"'
        filepath = "test.cs"
        regex = "[^a-zA-Z0-9!@#$%^&*()-=_+,.?\"':{}|<>_\[\]\\ \r\n]"

        texts = []

        # open the file
        with open( filepath, 'r' ) as f:
            for lineNum, line in enumerate(f, 1):
                results = search.GatherStringifyTextWithSingleDelimiter( line, delimiter )
                if( 0 != len(results) ):
                    texts.extend( results ) 
            # end file content loop
        # end of file open

        print( "texts: " )
        print( texts )


        results = search.GatherStringInstance(texts, regex)
        print( "results: " )
        print( results )

        print( str( len( results ) ) + " vs " + str( len( texts ) ) )
    # end of function; test_SearchForNonEnglishTextInFile
# end of class; SearchTestCase
