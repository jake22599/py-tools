# -*- coding: utf-8 -*-
import os, re
from shutil import copy2
import constant

def GatherContentRecursively(path, file_regex_filter = None):
    contents = []

    # r=root, d=directories, f = file_paths
    # loop through all the folders recursively
    for r, d, f in os.walk(path):
        # loop through all the files in the folder
        for file in f:
            file_path = os.path.join(r, file);

            # if there
            if( file_regex_filter is not None ):
                # ONLY look for file_paths that end with regular expression
                regex_result = re.search(file_regex_filter, file)
                if ( regex_result ):
                    contents.append( file_path )
            else: 
                contents.append( file_path )
    return contents
# function end; GatherContentRecursively

def StripRelativePath(array, relative):
    newArray = []
    for path in array:
        newPath = path.replace( relative, "" )
        newArray.append( newPath );
    return newArray
# function end; StripRelativePath

def SplitFoldersAndFiles(sourceContents, otherContents, otherRelativepath):
    missingContents = []
    missingFolders = {}

    for sourceContent in sourceContents:
        if ( sourceContent not in otherContents ):
            missingContents.append( sourceContent );
            # print( sourceContent );

            # check if the folder for this path exist
            folderIndexPos = sourceContent.rfind( constant.PATH_SEPARATOR );
            folderPath = sourceContent[0:folderIndexPos]
            folderFullPath = otherRelativepath + constant.PATH_SEPARATOR + folderPath

            # check if the folder exist
            while ( folderIndexPos is not -1 and os.path.isdir( folderFullPath ) is False ):
                # add this folder to path
                missingFolders[ folderPath ] = 1

                # check if the folder for this path exist
                folderIndexPos = folderPath.rfind( constant.PATH_SEPARATOR );
                folderPath = folderPath[0:folderIndexPos]
                folderFullPath = otherRelativepath + constant.PATH_SEPARATOR + folderPath

    return missingFolders, missingContents
# function end; SplitFoldersAndFiles

#########################################################################################################
# WORK IN PROGRESS
#########################################################################################################
