# -*- coding: utf-8 -*-
import shutil, os
import unittest

import comparison

class ComparisonTestCase(unittest.TestCase):
    # class "FAKE" constants
    TEST_FOLDER_NAME = "unit_test"

    def setUp(self):
        print( "[SETUP]  ComparisonTestCase" )

        # create a sub folder for testing
        os.mkdir( self.TEST_FOLDER_NAME )

        # define the folder paths
        rootFolder = self.TEST_FOLDER_NAME

        # create the test case environment
        testFolders = [
            rootFolder + "/test1",
            rootFolder + "/test1/test01",
            rootFolder + "/test1/test01/test002",
            rootFolder + "/test2",
            rootFolder + "/test2/test01",
            rootFolder + "/test2/test01/test003"
        ]
        
        testFiles = [
            rootFolder + "/test1/demofile1.txt",
            rootFolder + "/test1/demofile2.txt",
            rootFolder + "/test1/test01/demofile3.txt",
            rootFolder + "/test1/test01/test002/demofile4.txt",
        ]

        # create the folders
        for folderPath in testFolders:
            os.mkdir( folderPath )

        # create the files
        for filePath in testFiles:
            # create content file in testing folder 01
            f = open(filePath, "w+")
            f.write("Woops! I have deleted the content!")
            f.close()
    # end of function; setUp

    def tearDown(self):
        print( "[TEARDOWN]  ComparisonTestCase" )

        # remove the testing sub folder
        shutil.rmtree( self.TEST_FOLDER_NAME )
    # end of function; tearDown

    ########################
    # UNIT TESTS
    ########################
    def test_GatherContentRecursively(self):
        print( "[TEST]  ComparisonTestCase.test_GatherContentRecursively" )

        # define the folder paths
        rootFolder = self.TEST_FOLDER_NAME

        # gather all files and folder in the source path
        contents = comparison.GatherContentRecursively( rootFolder )
        # print( "contents: " )
        # print( contents )

        # there should have some content in the array
        self.assertIsNotNone(contents, "contents is none")
    # end of function; test_GatherContentRecursively

    def test_StripRelativePath(self):
        print( "[TEST]  ComparisonTestCase.test_StripRelativePath" )

        parent = "/home/user/me/work"
        source = parent + "/python"
        sourceArray = [ source ]

        #
        resultArray = comparison.StripRelativePath( sourceArray, parent )

        # ensure the object is not None
        self.assertIsNotNone(resultArray, "contents is none")
    # end of function; test_StripRelativePath

    def test_SplitFoldersAndFiles(self):
        print( "[TEST]  ComparisonTestCase.test_SplitFoldersAndFiles" )
        
        # define the folder paths
        rootFolder = self.TEST_FOLDER_NAME
        testPath1 = rootFolder + "/test1"
        testPath2 = rootFolder + "/test2"

        # gather all files and folder in the source path
        contents1 = comparison.GatherContentRecursively( testPath1 )
        contents2 = comparison.GatherContentRecursively( testPath2 )
        print( "contents1: " )
        print( contents1 )
        print( "contents2: " )
        print( contents2 )

        folders, files = comparison.SplitFoldersAndFiles( contents1, contents2, rootFolder )
        print( "folders: " )
        print( folders )
        print( "files: " )
        print( files )
        
        print( "########################################################" )

        # there should have some content in the array
        self.assertIsNotNone(folders, "folders is none")
        self.assertIsNotNone(files, "files is none")
    # end of function; test_SplitFoldersAndFiles
# end of class; ComparisonTestCase
 
if __name__ == '__main__':
    unittest.main()
    